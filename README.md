# Ticketron

Questo componente fa parte del sistema SegnalaCi, prodotto utilizzato dal Comune di Genova per offrire un servizio di segnalazioni ai cittadini.

## Cosa fa
Questo servizio si occupa di fare da sincronizzatore tra la Stanza Del Cittadino e il Sistema di segnalazioni del Comune di Genova:
In particolare esso sincronizza:
- le segnalazioni aperte lato Stanza del Cittadino, importandole sul CzRM del comune di Genova
- gli utenti creati/aggiornati lato Stanza del Cittadino, creando il relativo account sul CzRM
- i messaggi inviati lato Stanza del Cittadino, importandoli sul CzRM
- gli allegati relativi ai messaggi inseriti dalla Stanza del Cittadino verso il CzRM
- le precedenti operazione ma nel verso opposto: segnalazioni, utenti, messaggi e allegati creati sul CzRM vengono importati sulla Stanza. 

## Struttura del repository
Il repository ha come unico riferimento il branch main. \

**main.go** - entrypoint del servizio. Qui vengono assemblate e avviate le strutture seguenti.\
**czrmApi/**: - contiene le API per la comunicazione con il CzRM \
**kafkaConsumer/**: - contiente i consumer per gli eventi di Kafka\
**kafkaEventHandlers/**: - contiente il codice che descrive gli usercase per ogni tipo di evento ricevuto\
**kafkaMessageHandler/**: - contiene gli oggetti che validano e convertono gli eventi in strutture utilizzabili dal servizio \
**kafkaMessagesStruct/**: - contiene le strutture dati che descrivono gli eventi di Kafka \
**config/** - contiene la struttura atta alla configurazione del servizio: variabili d'ambiente, configurazione hardcoded\
**kafkaProducer/** - contiene il producer per gli eventi di Kafka\
**sentryUtils/** - contiene la configurazione di Sentry per il monitoring degli errori\
**logger/** - contiene la configurazione del sistema di logs in formato in formato Apache\
**sdcApi/** - contiene le API per la comunicazione con la Stanza Del Cittadino) \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori(kafka, kafka-ui, ksqldb-server, ksqldb-cli, zookeeper..) 

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori





`## Configurazione
### Configurazione variabili d'ambiente
| Nome                             | Default | Descrizione                                                             |
|----------------------------------|---------|-------------------------------------------------------------------------|
| KAFKA_SERVER                     | test    | Indirizzo del broker kafka per connetersi al cluster                    |
| KAFKA_APPLICATIONS_TOPIC         | test    | Nome del topic "applications"                                           |
| KAFKA_APPLICATION_CONSUMER_GROUP | test    | consumer group id del topic "applications"                              |
| KAFKA_USERS_TOPIC                | test    | Nome del topic "users"                                                  |
| KAFKA_USERS_CONSUMER_GROUP       | test    | consumer group id del topic "users"                                     |
| KAFKA_MESSAGES_TOPIC             | test    | Nome del topic "messages"                                               |
| KAFKA_MESSAGES_CONSUMER_GROUP    | test    | consumer group id del topic "messages"                                  |
| KAFKA_CZRMUPDATE_TOPIC           | test    | Nome del topic "messages"                                               |
| KAFKA_CZRMUPDATE_CONSUMER_GROUP  | test    | consumer group id del topic "messages"                                  |
| KAFKA_RETRY_QUEUE_TOPIC          | test    | nome del topic di destinazione per sfruttare il meccanismo di retry     |
| KAFKA_RETRY_QUEUE_CONSUMER_GROUP | test    | consumer group id del topic "retryQueue                                 |
| SENTRY_DSN                       | test    | url del dns di Sentry                                                   |
| CZRM_BASENAME                    | test    | base url del CzRM per l'utilizzo delle relative API                     |
| CZRM_API_VERSION                 | test    | versione delle API esposte dal CzRM                                     |
| CZRM_AUTH_TYPE                   | test    | seleziona il tipo di autenticazione verso le API del CzRM               |
| CZRM_AUTH_TOKEN_ENDPOINT         | test    | endpoint specifico per l'autenticazione tramite le API del CzRM         |
| CZRM_AUTH_TOKEN_USER             | test    | username per l'autenticazione delle API del CzRM                        |
| CZRM_AUTH_TOKEN_PASSWORD         | test    | password per l'autenticazione delle API del CzRM                        |
| CZRM_AUTH_TOKEN_GRANT_TYPE       | test    | specifica il grant type utilizzato dalla API di autenticazione del CzRM |
| CZRM_AUTH_TOKEN_CLIENT_ID        | test    | client_id specifico per la API di autenticazione del CzRM               |
| CZRM_AUTH_TOKEN_CLIENT_SECRET    | test    | client_secret specifico per la API di autenticazione del CzRM           |
| SDC_BASENAME                     | test    | base Url della Stanza del Cittadino                                     |
| SDC_AUTH_TOKEN_USER              | test    | username per l'autenticazione delle API della Stanza Del Cittadino      |
| SDC_AUTH_TOKEN_PASSWORD          | test    | password per l'autenticazione delle API della Stanza Del Cittadino      |
| SDC_CATEGORIES_BASENAME          | test    | endpoint per recuperare la lista delle categorie del cittadino in ITA   |
| SERVICE_IDS_TO_PROCESS           | test    | lista dei service_id per i quali processeremo gli eventi                |
| TENANTS_IDS_TO_PROCESS           | test    | lista dei tenants per i quali processeremo gli eventi                   |


## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '<your-value>'
services:
  ticketron:
    build:
      context: '<your-value>'
    environment:
      KAFKA_SERVER: '<your-value>'
      KAFKA_APPLICATIONS_TOPIC: '<your-value>'
      KAFKA_APPLICATION_CONSUMER_GROUP: '<your-value>'
      KAFKA_USERS_TOPIC: '<your-value>'
      KAFKA_USERS_CONSUMER_GROUP: '<your-value>'
      KAFKA_MESSAGES_TOPIC: '<your-value>'
      KAFKA_MESSAGES_CONSUMER_GROUP: '<your-value>'
      KAFKA_CZRMUPDATE_TOPIC: '<your-value>'
      KAFKA_CZRMUPDATE_CONSUMER_GROUP: '<your-value>'
      KAFKA_RETRY_QUEUE_TOPIC: '<your-value>'
      KAFKA_RETRY_QUEUE_CONSUMER_GROUP: '<your-value>'

      SENTRY_DSN: '<your-value>'

      CZRM_BASENAME: '<your-value>'
      CZRM_API_VERSION: '<your-value>'
      CZRM_AUTH_TYPE: '<your-value>'
      CZRM_AUTH_TOKEN_ENDPOINT: '<your-value>'
      CZRM_AUTH_TOKEN_USER: '<your-value>'
      CZRM_AUTH_TOKEN_PASSWORD: '<your-value>'
      CZRM_AUTH_TOKEN_GRANT_TYPE: '<your-value>'
      CZRM_AUTH_TOKEN_CLIENT_ID: '<your-value>'
      CZRM_AUTH_TOKEN_CLIENT_SECRET: '<your-value>'

      SDC_BASENAME: '<your-value>'
      SDC_AUTH_TOKEN_USER: '<your-value>'
      SDC_AUTH_TOKEN_PASSWORD: '<your-value>'
      SDC_CATEGORIES_BASENAME: '<your-value>'
      SDC_OPERATOR_ID: '<your-value>'

      SERVICE_IDS_TO_PROCESS: '<your-value>'
      TENANTS_IDS_TO_PROCESS: '<your-value>'
```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/ticketron.git`
2. `cd ticketron`
3. `docker-compose build`
4. `docker compose up -d`

## Testing
sono presenti unit test. per lanciarli, utilizzare il commando `go test .`

## Stadio di sviluppo

il software si trova in produzione alla versione 1.0.3


## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it



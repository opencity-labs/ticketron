package czrmrequeststructs

type CreateAccountInputValues struct {
	FirstName           string
	LastName            string
	Fiscalcode          string
	Email               string
	ShippingStreet      string
	ShippingState       string
	ShippingPostalCode  string
	ShippingCountryCode string
	SdcAccountId        string
	Phone               string
}

type CreateAccountResponse struct {
	CreateAccountSuccessResponse CreateAccountSuccessResponse
	CreateAccountBadResponse     []CreateAccountBadResponse
}

type CreateAccountSuccessResponse struct {
	ID      string        `json:"id"`
	Success bool          `json:"success"`
	Errors  []interface{} `json:"errors"`
}
type CreateAccountBadResponse struct {
	Message   string        `json:"message"`
	ErrorCode string        `json:"errorCode"`
	Fields    []interface{} `json:"fields"`
}

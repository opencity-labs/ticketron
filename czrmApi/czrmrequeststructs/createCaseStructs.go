package czrmrequeststructs

type CreateCaseinputValues struct {
	Origin              string  `json:"Origin"`
	Subject             string  `json:"Subject"`
	Description         string  `json:"Description"`
	PrivacyC            bool    `json:"Privacy__c"`
	CategoriaR          string  `json:"Categoria__r"`
	CategoriaCittadino  string  `json:"Categoria_del_Cittadino__r"`
	AccountID           string  `json:"AccountId"`
	PostCode            string  `json:"Indirizzo__PostalCode__s"`
	Address             string  `json:"Indirizzo__Street__s"`
	City                string  `json:"Indirizzo__City__s"`
	IndirizzoLatitudeS  float64 `json:"Indirizzo__Latitude__s"`
	IndirizzoLongitudeS float64 `json:"Indirizzo__Longitude__s"`
	ApplicationId       string  `json:"SegnalaCiId__C"`
	PdfLink             string  `json:"Riepilogo_Case_Migrato__c"`
	Municipio           string  `json:"Municipio__c"`
	CreatedAt           string  `json:"Data_Apertura_Case__c"`
	ContactId           string  `json:"ContactId"`
}

type CreateCaseResponse struct {
	CreateCaseSuccessResponse CreateCaseSuccessResponse
	CreateCaseBadResponse     []CreateCaseBadResponse
}

type CreateCaseSuccessResponse struct {
	ID      string        `json:"id"`
	Success bool          `json:"success"`
	Errors  []interface{} `json:"errors"`
}
type CreateCaseBadResponse struct {
	Message   string        `json:"message"`
	ErrorCode string        `json:"errorCode"`
	Fields    []interface{} `json:"fields"`
}

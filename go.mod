module opencitylabs.it/ticketron

go 1.21


require (
	github.com/getsentry/sentry-go v0.29.1
	github.com/go-playground/validator/v10 v10.23.0
	github.com/segmentio/kafka-go v0.4.47
	go.uber.org/zap v1.27.0
)

require (
	github.com/gabriel-vasile/mimetype v1.4.7 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/klauspost/compress v1.17.11 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.21 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.29.0 // indirect
	golang.org/x/net v0.31.0 // indirect
	golang.org/x/sys v0.27.0 // indirect
	golang.org/x/text v0.20.0 // indirect
)

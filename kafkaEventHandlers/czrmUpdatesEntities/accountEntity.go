package czrmupdatesentities

import (
	"errors"
	"net/http"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IAccountEntity interface {
	CreateOrUpdateAccountOnSdc(m kafkaMessagesStruct.CzrmUpdatesMessageStruct) error
	SetSentryHub(s *sentry.Hub)
}

type accountEntity struct {
	logger          *zap.Logger
	conf            *config.Config
	czrmApi         czrmapi.ICzrmApi
	message         kafkaMessagesStruct.CzrmUpdatesMessageStruct
	czrmAccountInfo czrmrequeststructs.GetAccountByCzrmIdSuccessResponse
	sdcApi          sdcapi.ISdcApi
	sentryHub       *sentry.Hub
}

func NewAccountEntity(log *zap.Logger, confing *config.Config, czrmapi czrmapi.ICzrmApi, sdcapi sdcapi.ISdcApi) IAccountEntity {
	return &accountEntity{
		logger:  log,
		conf:    confing,
		czrmApi: czrmapi,
		sdcApi:  sdcapi,
	}
}

func (ev *accountEntity) SetSentryHub(s *sentry.Hub) {
	ev.sentryHub = s
}

func (ev *accountEntity) CreateOrUpdateAccountOnSdc(m kafkaMessagesStruct.CzrmUpdatesMessageStruct) error {
	ev.message = m
	token, err := ev.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> AccessAuthTokenRequest returned an error for user topic ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("MESSAGES-> error retrieving sdc auth token"))
		return err
	}
	var statuscode int
	ev.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().SetInputValues(ev.getGetCzrmAccountByCzrmIdInputValues())
	ev.czrmAccountInfo, statuscode, err = ev.czrmApi.AccessGetCzrmAccountByCzrmIdRequest().GetAccountByCzrmIdRequest(token)
	if err != nil || statuscode != http.StatusOK {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error retrieving Czrm Account from CzRM: ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error retrieving Czrm Account from CzRM"))
		return err
	}
	ev.logger.Sugar().Debug("CZRM-UPDATES-ACCOUNT -> SDC id: ", ev.czrmAccountInfo.SegnalaCiIDC)
	if ev.czrmAccountInfo.SegnalaCiIDC == "" {
		ev.logger.Debug("CZRM-UPDATES-ACCOUNT -> found new account on czrm. We need to import it on SDC ")
		sdcAccountInfo, err := ev.CreateUserOnSdc()
		if err != nil {
			ev.logger.Sugar().Error("CZRM-UPDATES-ACCOUNT -> error creating new account on Sdc:  ", ev.czrmAccountInfo.SegnalaCiIDC)
			ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error creating new account on Sdc"))
			return err
		}
		ev.logger.Sugar().Debug("CZRM-UPDATES-ACCOUNT -> New account created on SDC. Id:  ", sdcAccountInfo.ID)
		err = ev.UpdateSegnalaCiIdOnAccountOnCzrm(sdcAccountInfo.ID)
		if err != nil {
			ev.logger.Sugar().Error("CZRM-UPDATES-ACCOUNT -> error update SegnalaciId On Czrm account new account on Sdc:  ")
			ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error update SegnalaciId On Czrm account new account on Sdc"))

			return err
		}
	} else {
		ev.logger.Sugar().Debug("CZRM-UPDATES-ACCOUNT -> SDC id found on czrm account, we need to update it: ", ev.czrmAccountInfo.SegnalaCiIDC)
		err = ev.UpdateAccountOnSdc(ev.czrmAccountInfo.SegnalaCiIDC)
		if err != nil {
			ev.logger.Sugar().Error("CZRM-UPDATES-ACCOUNT -> error updating account on Sdc:  ", ev.czrmAccountInfo.SegnalaCiIDC)
			ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error updating account on Sdc"))

			return err
		}
		ev.logger.Sugar().Debug("CZRM-UPDATES-ACCOUNT ->  account updated on SDC. Id:  ", ev.czrmAccountInfo.SegnalaCiIDC)
	}
	ev.logger.Sugar().Info("CZRM-UPDATES-ACCOUNT -> processed event: "+ev.message.EventId+"created or updated user on Sdc with the following id: ", ev.czrmAccountInfo.SegnalaCiIDC)
	return nil
}

func (ev *accountEntity) getGetCzrmAccountByCzrmIdInputValues() czrmrequeststructs.GetAccountByCzrmIdInputValues {
	return czrmrequeststructs.GetAccountByCzrmIdInputValues{
		User: ev.message.ID,
	}
}
func (ev *accountEntity) CreateUserOnSdc() (sdcapistructs.UserInfo, error) {
	var sdcAccountInfo sdcapistructs.UserInfo
	token, err := ev.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc: ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc"))
		return sdcAccountInfo, err
	}
	ev.sdcApi.AccessCreateUserRequest().SetInputValues(ev.getCreateSdcUserinputValues())
	sdcAccountInfo, err = ev.sdcApi.AccessCreateUserRequest().CreateUser(token)
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error creating new user on sdc:", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error creating new user on sdc"))
		return sdcAccountInfo, err
	}
	return sdcAccountInfo, nil
}
func (ev *accountEntity) UpdateAccountOnSdc(sdcAccountId string) error {
	token, err := ev.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc: ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc"))
		return err
	}
	ev.sdcApi.AccessUpdateUserbyId().SetInputValues(ev.UpdateUserByIdInputValues())
	_, err = ev.sdcApi.AccessUpdateUserbyId().UpdateUserById(token, sdcAccountId)
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error updating user on sdc:", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error updating user on sdc"))

		return err
	}
	return nil
}
func (ev *accountEntity) UpdateSegnalaCiIdOnAccountOnCzrm(sdcAccountId string) error {
	token, err := ev.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc: ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error retrieving auth token form sdc"))
		return err
	}
	ev.czrmApi.AccessUpdateAccountSegnalaciIdRequest().SetInputValues(ev.getUpdateSegnalaCiIdCzrmAccountInputValues(sdcAccountId))
	_, err = ev.czrmApi.AccessUpdateAccountSegnalaciIdRequest().UpdateAccountSegnalaciId(token, ev.message.ID)
	if err != nil {
		ev.logger.Error("CZRM-UPDATES-ACCOUNT -> error trying to update segnalaciId__c field on czrm: ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("CZRM-UPDATES-ACCOUNT -> error trying to update segnalaciId__c field on czrm"))

		return err
	}
	return nil
}
func (ev *accountEntity) getUpdateSegnalaCiIdCzrmAccountInputValues(sdcAccountId string) czrmrequeststructs.UpdateAccountInputValues {
	return czrmrequeststructs.UpdateAccountInputValues{
		SdcAccountId: sdcAccountId,
	}
}
func (ev *accountEntity) UpdateUserByIdInputValues() sdcapistructs.UpdateUserByIdInputValues {
	return sdcapistructs.UpdateUserByIdInputValues{
		Nome:          ev.czrmAccountInfo.FirstName,
		Cognome:       ev.czrmAccountInfo.LastName,
		Cellulare:     ev.czrmAccountInfo.Phone,
		Email:         ev.czrmAccountInfo.PersonEmail,
		Telefono:      ev.czrmAccountInfo.Phone,
		CodiceFiscale: ev.czrmAccountInfo.CodiceFiscaleC,
	}
}
func (ev *accountEntity) getCreateSdcUserinputValues() sdcapistructs.CreateUserInputValues {
	return sdcapistructs.CreateUserInputValues{
		Nome:               ev.czrmAccountInfo.FirstName,
		Cognome:            ev.czrmAccountInfo.LastName,
		Cellulare:          ev.czrmAccountInfo.Phone,
		Email:              ev.czrmAccountInfo.PersonEmail,
		CodiceFiscale:      ev.czrmAccountInfo.CodiceFiscaleC,
		DataNascita:        ev.czrmAccountInfo.PersonBirthdate,
		Telefono:           ev.czrmAccountInfo.Phone,
		IndirizzoDomicilio: ev.czrmAccountInfo.ShippingStreet,
		CapDomicilio:       ev.czrmAccountInfo.ShippingPostalCode,
		CittaDomicilio:     ev.czrmAccountInfo.ShippingState,
		ProvinciaDomicilio: ev.czrmAccountInfo.ShippingStateCode,
		StatoDomicilio:     ev.czrmAccountInfo.ShippingCountry,
		IndirizzoResidenza: ev.czrmAccountInfo.BillingAddress.Street,
		CapResidenza:       ev.czrmAccountInfo.BillingAddress.PostalCode,
		CittaResidenza:     ev.czrmAccountInfo.BillingAddress.City,
		ProvinciaResidenza: ev.czrmAccountInfo.BillingAddress.StateCode,
		StatoResidenza:     ev.czrmAccountInfo.BillingAddress.Country,
	}
}

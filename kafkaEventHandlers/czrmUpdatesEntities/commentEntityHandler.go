package czrmupdatesentities

import (
	"errors"
	"net/http"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
	sdcapi "opencitylabs.it/ticketron/sdcApi"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type ICommentEntity interface {
	AddMessageOnApplication(message kafkaMessagesStruct.CzrmUpdatesMessageStruct) error
	SetSentryHub(s *sentry.Hub)
}

type commentEntity struct {
	logger    *zap.Logger
	conf      *config.Config
	czrmApi   czrmapi.ICzrmApi
	sdcApi    sdcapi.ISdcApi
	sentryHub *sentry.Hub
}

func NewCommentEntity(log *zap.Logger, confing *config.Config, czrmapi czrmapi.ICzrmApi, sdcapi sdcapi.ISdcApi) ICommentEntity {
	return &commentEntity{
		logger:  log,
		conf:    confing,
		czrmApi: czrmapi,
		sdcApi:  sdcapi,
	}
}
func (ce *commentEntity) SetSentryHub(s *sentry.Hub) {
	ce.sentryHub = s
}

func (ce *commentEntity) AddMessageOnApplication(message kafkaMessagesStruct.CzrmUpdatesMessageStruct) error {
	ce.logger.Sugar().Debug("CZRM-UPDATES-COMMENT -> processing comment")
	token, err := ce.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> AccessAuthTokenRequest returned an error ", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> AccessAuthTokenRequest returned an error"))

		return err
	}
	comment, _, err := ce.czrmApi.AccessGetCommentRequest().GetCommentRequest(token, message.ID)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error retrieving comment ", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error retrieving comment"))

		return err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-COMMENT -> comment: ", comment.DescriptionC)
	application, err := ce.getApplicationByExternalId(comment.CaseIDC)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error retrieving application by external id  ", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error retrieving application by external id"))

		return err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-COMMENT -> related application: ", application.ID)
	ce.createMessageOnApplication(application.ID, comment.DescriptionC, message.UpdatedAt, comment.ID)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error creating message on application ", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error creating message on application "))

		return err
	}
	ce.logger.Sugar().Info("CZRM-UPDATES-COMMENT -> processed event: "+message.EventId+" . created new message on application: ", application.ID)
	return nil
}

func (ce *commentEntity) getApplicationByExternalId(externalId string) (sdcapistructs.GetApplicationByExternalIdIdSuccessResponse, error) {
	application := sdcapistructs.GetApplicationByExternalIdIdSuccessResponse{}
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error retrieving sdc auth token", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error retrieving sdc auth token"))

		return application, err
	}
	application, statuscode, _ := ce.sdcApi.AccessGetApplicationByExternaIdRequest().GetApplicationByExternalIdId(token, externalId)
	if statuscode == http.StatusNotFound {
		ce.logger.Debug("CZRM-UPDATES-COMMENT -> application not found on sdc")
		return application, err
	}
	return application, nil
}

func (ce *commentEntity) createMessageOnApplication(applicationId, DescriptionC, sentAt, czrmCommentId string) error {
	token, err := ce.sdcApi.AccessAuthTokenRequest().GetAuthToken()
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error retrieving sdc auth token", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error retrieving sdc auth token"))

		return err
	}
	ce.sdcApi.AccessCreateMessage().SetInputValues(ce.getCreateMessageInputValues(DescriptionC, sentAt, czrmCommentId))
	err = ce.sdcApi.AccessCreateMessage().CreateMessage(token, applicationId)
	if err != nil {
		ce.logger.Error("CZRM-UPDATES-COMMENT -> error adding commenti to application", zap.Error(err))
		ce.sentryHub.CaptureException(errors.New("CZRM-UPDATES-COMMENT -> error adding commenti to application"))

		return err
	}
	ce.logger.Sugar().Debug("CZRM-UPDATES-COMMENT -> application correctly updated with new comment. application: ", applicationId)
	return nil
}

func (ce *commentEntity) getCreateMessageInputValues(message, sentAt, czrmCommentId string) sdcapistructs.CreateMessageInputValues {
	return sdcapistructs.CreateMessageInputValues{
		Message:    message,
		SentAt:     sentAt,
		ExternalID: czrmCommentId,
	}
}

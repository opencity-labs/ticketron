package kafkaEventHandlers

import (
	"errors"
	"net/http"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	czrmapi "opencitylabs.it/ticketron/czrmApi"
	"opencitylabs.it/ticketron/czrmApi/czrmrequeststructs"
	"opencitylabs.it/ticketron/kafkaMessagesStruct"
)

type IUserEventHandler interface {
	SetStructuredMessageFromUserEvent(message kafkaMessagesStruct.UsersMessageStruct)
	ProcessMessage() error
	ShouldPutMessagetoRetryQueue() bool
	createOrUpdateUser()
	shouldProcesEvent() bool
	shouldProcesThisTenants() bool
	SetSentryHub(*sentry.Hub)
}

type userEventHandler struct {
	logger                       *zap.Logger
	conf                         *config.Config
	czrmApi                      czrmapi.ICzrmApi
	message                      kafkaMessagesStruct.UsersMessageStruct
	shouldPutMessagetoRetryQueue bool
	sentryHub                    *sentry.Hub
}

func NewUserEventHandler(log *zap.Logger, config *config.Config) IUserEventHandler {
	ev := &userEventHandler{
		logger:                       log,
		conf:                         config,
		shouldPutMessagetoRetryQueue: false,
		czrmApi:                      czrmapi.NewCzrmApi(log, config),
	}

	return ev
}

func (ev *userEventHandler) SetStructuredMessageFromUserEvent(message kafkaMessagesStruct.UsersMessageStruct) {
	ev.message = message
}
func (ev *userEventHandler) SetSentryHub(s *sentry.Hub) {
	ev.sentryHub = s
}
func (ev *userEventHandler) ProcessMessage() error {
	ev.shouldPutMessagetoRetryQueue = false
	if !ev.shouldProcesEvent() {
		ev.logger.Sugar().Debug("USERS -> This User shouldn't be processed")
		return nil
	}
	ev.createOrUpdateUser()
	ev.cleanResources()
	return nil
}
func (ev *userEventHandler) ShouldPutMessagetoRetryQueue() bool {
	return ev.shouldPutMessagetoRetryQueue
}

func (ev *userEventHandler) createOrUpdateUser() {
	token, err := ev.czrmApi.AccessAuthTokenRequest().GetNewAuthToken()
	if err != nil {
		ev.logger.Error("USER -> AccessAuthTokenRequest returned an error for user topic ", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("USER -> AccessAuthTokenRequest returned an error for user topic"))
		ev.shouldPutMessagetoRetryQueue = true
		return
	}
	czrmAccountId, responseStatus, err := ev.GetCzrmAccountBySdcAccountIdRequest(token)
	if responseStatus == http.StatusNotFound || err != nil {
		czrmAccountId = ev.createAccountRequest(token)
		if czrmAccountId == "" {
			ev.shouldPutMessagetoRetryQueue = true
			return
		}
		return
	}
	if responseStatus == http.StatusOK {
		ev.logger.Sugar().Debug("USER -> user found, we need to update it: ", czrmAccountId)
		err = ev.updateAccountRequest(token, czrmAccountId)
		if err != nil {
			ev.shouldPutMessagetoRetryQueue = true
			return
		}

	}
}

func (ev *userEventHandler) GetCzrmAccountBySdcAccountIdRequest(token string) (string, int, error) {
	czrmAccountId, responseStatus, err := ev.czrmApi.AccessGetCzrmAccountBySdcAccountIdRequest().GetCzrmAccountIdBySdcAccountIdRequest(token, ev.message.ID)
	if err != nil {
		ev.logger.Error("USER -> GetCzrmAccountBySdcAccountIdRequest error performing the request", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("USER -> GetCzrmAccountBySdcAccountIdRequest error performing the request"))
	}
	return czrmAccountId, responseStatus, err
}

func (ev *userEventHandler) createAccountRequest(token string) string {
	ev.logger.Debug("USER -> account not present on CzRM. we need to create it...")
	ev.czrmApi.AccessCreateAccountRequest().SetInputValues(ev.getCreateAccountInputValues())
	czrmAccountId, _, err := ev.czrmApi.AccessCreateAccountRequest().CreateNewAccount(token)
	if err != nil {
		ev.logger.Error("USER -> CreateAccount returned an error", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("USER -> CreateAccount returned an error"))
		return ""
	}
	return czrmAccountId
}

func (ev *userEventHandler) updateAccountRequest(token string, czrmAccountId string) error {
	ev.czrmApi.AccessUpdateAccountRequest().SetInputValues(ev.getUpdateAccountInputValues())
	_, err := ev.czrmApi.AccessUpdateAccountRequest().UpdateAccount(token, czrmAccountId)
	if err != nil {
		ev.logger.Error("USER -> UdateAccount returned an error", zap.Error(err))
		ev.sentryHub.CaptureException(errors.New("USER -> UdateAccount returned an error"))
		return err
	}
	return nil
}

func (ch *userEventHandler) getCreateAccountInputValues() czrmrequeststructs.CreateAccountInputValues {
	return czrmrequeststructs.CreateAccountInputValues{
		FirstName:    ch.message.GivenName,
		LastName:     ch.message.FamilyName,
		Fiscalcode:   ch.czrmApi.GetValidOrNullfiscalCode(ch.message.TaxCode),
		Email:        ch.message.Email,
		SdcAccountId: ch.message.ID,
		Phone:        ch.czrmApi.GetUserPhone(ch.message.MobilePhone, ch.message.Phone),
	}
}

func (ch *userEventHandler) getUpdateAccountInputValues() czrmrequeststructs.UpdateAccountInputValues {
	return czrmrequeststructs.UpdateAccountInputValues{
		FirstName:    ch.message.GivenName,
		LastName:     ch.message.FamilyName,
		Fiscalcode:   ch.czrmApi.GetValidOrNullfiscalCode(ch.message.TaxCode),
		Email:        ch.message.Email,
		SdcAccountId: ch.message.ID,
		Phone:        ch.czrmApi.GetUserPhone(ch.message.MobilePhone, ch.message.Phone),
	}
}

func (ev *userEventHandler) shouldProcesEvent() bool {
	return ev.shouldProcesThisTenants() && ev.message.Role == "user"
}

func (ev *userEventHandler) shouldProcesThisTenants() bool {

	for _, tenantID := range ev.conf.TenantsIdToProcess {
		if tenantID == ev.message.TenantID {
			return true
		}
	}
	return false
}

func (ev *userEventHandler) cleanResources() {
	ev.message = kafkaMessagesStruct.UsersMessageStruct{}
}

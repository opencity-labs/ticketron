package kafkaMessagesStruct

type MessageMessageStruct struct {
	EventId           string       `json:"event_id"`
	AuthorId          string       `json:"author_id"`
	ExternalID        string       `json:"external_id"`
	ID                string       `json:"id"`
	RelatedEntityId   string       `json:"related_entity_id"`
	RelatedEntityType string       `json:"related_entity_type"`
	Message           string       `json:"message"`
	TenantID          string       `json:"tenant_id"`
	TransmissionType  string       `json:"transmission_type"`
	Attachments       []Attachment `json:"attachments"`
}

type Attachment struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"original_name"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

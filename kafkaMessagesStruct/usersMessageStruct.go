package kafkaMessagesStruct

type UsersMessageStruct struct {
	EventId     string   `json:"event_id"`
	AppID       string   `json:"app_id"`
	AppVersion  string   `json:"app_version"`
	ID          string   `json:"id"`
	Domicile    Domicile `json:"domicile"`
	Email       string   `json:"email"`
	FamilyName  string   `json:"family_name"`
	FullName    string   `json:"full_name"`
	GivenName   string   `json:"given_name"`
	Role        string   `json:"role"`
	TaxCode     string   `json:"tax_code"`
	TenantID    string   `json:"tenant_id"`
	Phone       string   `json:"telephone"`
	MobilePhone string   `json:"mobile_phone"`
}

type Domicile struct {
	Address      string `json:"address"`
	Country      string `json:"country"`
	County       string `json:"county"`
	Municipality string `json:"municipality"`
	PostCode     string `json:"post_code"`
}

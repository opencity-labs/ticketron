package sdcapirequests

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"go.uber.org/zap"
	"opencitylabs.it/ticketron/config"
	sdcapistructs "opencitylabs.it/ticketron/sdcApi/sdcApiStructs"
)

type IApplicationChangeStatus interface {
	SetInputValues(input sdcapistructs.ApplicationChangeStatusInputValues)
	ApplicationChangeStatus(token, applicationId string) (bool, error)
}

type applicationChangeStatus struct {
	logger         *zap.Logger
	conf           *config.Config
	client         *http.Client
	inputValues    sdcapistructs.ApplicationChangeStatusInputValues
	responseValues sdcapistructs.ApplicationChangeStatusResponse
}

func NewApplicationChangeStatus(log *zap.Logger, config *config.Config) IApplicationChangeStatus {
	return &applicationChangeStatus{
		logger: log,
		conf:   config,
		client: &http.Client{},
	}
}
func (r *applicationChangeStatus) SetInputValues(input sdcapistructs.ApplicationChangeStatusInputValues) {
	r.inputValues = input
}
func (r *applicationChangeStatus) ApplicationChangeStatus(token, applicationId string) (bool, error) {
	err := r.doApplicationChangeStatusRequest(token, applicationId)
	if err != nil {
		return false, err
	}
	defer r.cleanResources()
	return true, nil
}

func (r *applicationChangeStatus) doApplicationChangeStatusRequest(token, applicationId string) error {

	req, err := r.buildRequest(token, applicationId)
	if err != nil {
		return err
	}

	// make the request
	resp, err := r.client.Do(req)
	if err != nil {
		r.logger.Sugar().Error("error performing ApplicationChangeStatus request: ", zap.Error(err))
		return err
	}
	defer resp.Body.Close()
	err = r.handleApplicationChangeStatusResponse(resp)
	if err != nil {
		return err
	}

	return nil
}
func (r *applicationChangeStatus) buildRequest(token, applicationId string) (*http.Request, error) {
	url := r.conf.Sdc.SdcBaseName + "applications" + applicationId + "/transition/assign"

	data := map[string]interface{}{
		"user_id":       r.inputValues.UserID,
		"user_group_id": r.inputValues.UserGroupID,
		"message":       r.inputValues.Message,
		"assigned_at":   r.inputValues.AssignedAt,
	}

	payload, err := json.Marshal(data)
	if err != nil {
		r.logger.Sugar().Error("Error Marshalling data ", zap.Error(err))
		return nil, err
	}
	r.logger.Sugar().Debug("ApplicationChangeStatus status payload: ", string(payload))
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		r.logger.Error("Error making new request ", zap.Error(err))
		return nil, err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	return req, nil
}
func (r *applicationChangeStatus) handleApplicationChangeStatusResponse(resp *http.Response) error {
	r.logger.Sugar().Debug("status code ApplicationChangeStatus: ", resp.StatusCode)
	if resp.StatusCode != http.StatusNoContent {
		err := json.NewDecoder(resp.Body).Decode(&r.responseValues.ApplicationChangeStatusBadResponse)
		if err != nil {
			r.logger.Error("error decoding body ApplicationChangeStatus response: ", zap.Error(err))
			return err
		}
		r.logger.Sugar().Debug("bad ApplicationChangeStatus response: ", r.responseValues.ApplicationChangeStatusBadResponse)
		r.logger.Sugar().Debug("bad ApplicationChangeStatus status code : ", resp.StatusCode)
		return errors.New("ApplicationChangeStatus bad error response")
	}
	r.logger.Debug("Application Status changed correctly")
	return nil
}

func (r *applicationChangeStatus) cleanResources() {
	r.logger.Sugar().Debug("applicationChangeStatus: cleaning resources after response")
	r.responseValues = sdcapistructs.ApplicationChangeStatusResponse{}
	r.inputValues = sdcapistructs.ApplicationChangeStatusInputValues{}

}

package sdcapistructs

type ApplicationChangeStatusInputValues struct {
	UserID      string `json:"user_id"`
	UserGroupID string `json:"user_group_id"`
	Message     string `json:"message"`
	AssignedAt  string `json:"assigned_at"`
}

type ApplicationChangeStatusResponse struct {
	ApplicationChangeStatusBadResponse ApplicationChangeStatusBadResponse
}

type ApplicationChangeStatusBadResponse struct {
	Type        string `json:"type"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

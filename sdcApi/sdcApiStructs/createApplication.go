package sdcapistructs

type CreateApplicationInputValues struct {
	AppID                     string            `json:"app_id"`
	AppVersion                string            `json:"app_version"`
	Attachments               []Attachments     `json:"attachments"`
	Authentication            Authentication    `json:"authentication"`
	BackofficeData            string            `json:"backoffice_data"`
	CompiledModules           []CompiledModules `json:"compiled_modules"`
	CreatedAt                 string            `json:"created_at"`
	CreationTime              int               `json:"creation_time"`
	Data                      Data              `json:"data"`
	EventCreatedAt            string            `json:"event_created_at"`
	EventID                   string            `json:"event_id"`
	EventVersion              int               `json:"event_version"`
	ExternalID                string            `json:"external_id"`
	FlowChangedAt             string            `json:"flow_changed_at"`
	GeographicAreas           GeographicAreas   `json:"geographic_areas"`
	ID                        string            `json:"id"`
	Integrations              []interface{}     `json:"integrations"`
	LatestStatusChangeAt      string            `json:"latest_status_change_at"`
	LatestStatusChangeTime    int               `json:"latest_status_change_time"`
	Links                     []Links           `json:"links"`
	Meetings                  []interface{}     `json:"meetings"`
	Outcome                   string            `json:"outcome"`
	OutcomeAttachments        []interface{}     `json:"outcome_attachments"`
	OutcomeFile               string            `json:"outcome_file"`
	OutcomeMotivation         string            `json:"outcome_motivation"`
	OutcomeProtocolDocumentID string            `json:"outcome_protocol_document_id"`
	OutcomeProtocolNumber     string            `json:"outcome_protocol_number"`
	OutcomeProtocolNumbers    string            `json:"outcome_protocol_numbers"`
	OutcomeProtocolTime       string            `json:"outcome_protocol_time"`
	OutcomeProtocolledAt      string            `json:"outcome_protocolled_at"`
	Path                      string            `json:"path"`
	PaymentData               []interface{}     `json:"payment_data"`
	PaymentType               string            `json:"payment_type"`
	ProtocolDocumentID        string            `json:"protocol_document_id"`
	ProtocolFolderCode        string            `json:"protocol_folder_code"`
	ProtocolFolderNumber      string            `json:"protocol_folder_number"`
	ProtocolNumber            string            `json:"protocol_number"`
	ProtocolNumbers           []ProtocolNumbers `json:"protocol_numbers"`
	ProtocolTime              string            `json:"protocol_time"`
	ProtocolledAt             string            `json:"protocolled_at"`
	Service                   string            `json:"service"`
	ServiceGroupName          string            `json:"service_group_name"`
	ServiceID                 string            `json:"service_id"`
	ServiceName               string            `json:"service_name"`
	SourceType                string            `json:"source_type"`
	Status                    string            `json:"status"`
	StatusName                string            `json:"status_name"`
	Subject                   string            `json:"subject"`
	SubmissionTime            int               `json:"submission_time"`
	SubmittedAt               string            `json:"submitted_at"`
	Tenant                    string            `json:"tenant"`
	TenantID                  string            `json:"tenant_id"`
	Timestamp                 string            `json:"timestamp"`
	User                      string            `json:"user"`
	UserCompilationNotes      string            `json:"user_compilation_notes"`
	UserName                  string            `json:"user_name"`
	XForwardedFor             string            `json:"x-forwarded-for"`
}

type Authentication struct {
	AuthenticationMethod string `json:"authentication_method"`
	Certificate          string `json:"certificate"`
	CertificateIssuer    string `json:"certificate_issuer"`
	CertificateSubject   string `json:"certificate_subject"`
	Instant              string `json:"instant"`
	SessionID            string `json:"session_id"`
	SessionIndex         string `json:"session_index"`
	SpidCode             string `json:"spid_code"`
	SpidLevel            string `json:"spid_level"`
}

type CompiledModules struct {
	CreatedAt        string `json:"created_at"`
	Description      string `json:"description"`
	ExternalID       string `json:"external_id"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolNumber   string `json:"protocol_number"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type Data struct {
	AddressAddressISO31662Lvl4            string   `json:"address.address.ISO3166-2-lvl4"`
	AddressAddressISO31662Lvl6            string   `json:"address.address.ISO3166-2-lvl6"`
	AddressAddressCity                    string   `json:"address.address.city"`
	AddressAddressCountry                 string   `json:"address.address.country"`
	AddressAddressCountryCode             string   `json:"address.address.country_code"`
	AddressAddressCounty                  string   `json:"address.address.county"`
	AddressAddressNeighbourhood           string   `json:"address.address.neighbourhood"`
	AddressAddressPostcode                string   `json:"address.address.postcode"`
	AddressAddressRoad                    string   `json:"address.address.road"`
	AddressAddressSuburb                  string   `json:"address.address.suburb"`
	AddressBoundingbox1                   string   `json:"address.boundingbox.1"`
	AddressBoundingbox2                   string   `json:"address.boundingbox.2"`
	AddressBoundingbox3                   string   `json:"address.boundingbox.3"`
	AddressClass                          string   `json:"address.class"`
	AddressDisplayName                    string   `json:"address.display_name"`
	AddressImportance                     float64  `json:"address.importance"`
	AddressLat                            float64  `json:"address.lat"`
	AddressLicence                        string   `json:"address.licence"`
	AddressLon                            float64  `json:"address.lon"`
	AddressOsmID                          int      `json:"address.osm_id"`
	AddressOsmType                        string   `json:"address.osm_type"`
	AddressPlaceID                        int      `json:"address.place_id"`
	AddressType                           string   `json:"address.type"`
	ApplicantDataCompletenameDataName     string   `json:"applicant.data.completename.data.name"`
	ApplicantDataCompletenameDataSurname  string   `json:"applicant.data.completename.data.surname"`
	ApplicantDataEmailAddress             string   `json:"applicant.data.email_address"`
	ApplicantDataPersonIdentifier         string   `json:"applicant.data.person_identifier"`
	ApplicantDataFiscalCodeDataFiscalCode string   `json:"applicant.data.fiscal_code.data.fiscal_code"`
	ApplicantDataPhoneNumber              string   `json:"applicant.data.phone_number"`
	Details                               string   `json:"details"`
	Docs                                  []Docs   `json:"docs"`
	Images                                []Images `json:"images"`
	Subject                               string   `json:"subject"`
	TypeLabel                             string   `json:"type.label"`
	TypeValue                             string   `json:"type.value"`
}

type Links struct {
	Action      string `json:"action"`
	Description string `json:"description"`
	URL         string `json:"url"`
}

type GeographicAreas struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Images struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type Docs struct {
	Description      string `json:"description"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	OriginalName     string `json:"originalName"`
	ProtocolRequired bool   `json:"protocol_required"`
	URL              string `json:"url"`
}

type ProtocolNumbers struct {
	ID         string `json:"id"`
	Protocollo string `json:"protocollo"`
}

type Attachments struct {
	CreatedAt        string      `json:"created_at"`
	Description      string      `json:"description"`
	ExternalID       string      `json:"external_id"`
	ID               string      `json:"id"`
	Name             string      `json:"name"`
	OriginalName     string      `json:"originalName"`
	ProtocolNumber   interface{} `json:"protocol_number"`
	ProtocolRequired bool        `json:"protocol_required"`
	URL              string      `json:"url"`
}

package sdcapistructs

// queste me le recupero chiamando le API
type GetApplicationByExternalIdIdInputValues struct {
	ApplicationId string
}

type GetApplicationByExternalIdIdResponse struct {
	GetApplicationByExternalIdIdSuccessResponse GetApplicationByExternalIdIdSuccessResponse
	GetApplicationByExternalIdIdBadResponse     GetApplicationByExternalIdIdBadResponse
}

type GetApplicationByExternalIdIdSuccessResponse struct {
	Type     string `json:"type"`
	Geometry struct {
		Type        string    `json:"type"`
		Coordinates []float64 `json:"coordinates"`
	} `json:"geometry"`
	ID string `json:"id"`
}

type GetApplicationByExternalIdIdBadResponse struct {
	Response []string
}

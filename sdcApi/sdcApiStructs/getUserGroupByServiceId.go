package sdcapistructs

import "time"

type GetUserGroupByServiceIdInputValues struct {
	ServiceId string
}

type GetUserGroupByServiceIdResponse struct {
	GetUserGroupByServiceIdSuccessResponse GetUserGroupByServiceIdSuccessResponse
	GetUserGroupByServiceIdBadResponse     GetUserGroupByServiceIdBadResponse
}

type GetUserGroupByServiceIdSuccessResponse struct {
	TopicID          string `json:"topic_id"`
	ManagerID        string `json:"manager_id"`
	UsersCount       int    `json:"users_count"`
	ServicesCount    int    `json:"services_count"`
	CoreLocationID   string `json:"core_location_id"`
	CalendarID       string `json:"calendar_id"`
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription string `json:"short_description"`
	MainFunction     string `json:"main_function"`
	MoreInfo         string `json:"more_info"`
	CoreContactPoint struct {
		ID          string    `json:"id"`
		Name        string    `json:"name"`
		Email       string    `json:"email"`
		URL         string    `json:"url"`
		PhoneNumber string    `json:"phone_number"`
		Pec         string    `json:"pec"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	} `json:"core_contact_point"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type GetUserGroupByServiceIdBadResponse struct {
	Response []string
}
